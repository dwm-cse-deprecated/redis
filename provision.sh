#! /bin/bash

set -ex

target_dir=`cd $(dirname ${0}) && pwd`

yum -y install tar
yum -y install make gcc-c++

cd /usr/local/src
curl -L http://download.redis.io/releases/redis-3.0.7.tar.gz -o redis-3.0.7.tar.gz
tar zxvf redis-3.0.7.tar.gz
cd redis-3.0.7
make && make install

mkdir /etc/redis
mkdir -p /var/lib/redis
mkdir /var/{run,log}/redis

cp ${target_dir}/configs/redis.conf /etc/redis/redis.conf
cp ${target_dir}/configs/init.d /etc/rc.d/init.d/redis

chmod 755 /etc/rc.d/init.d/redis
chkconfig --add redis
